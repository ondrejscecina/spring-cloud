package sk.itech.spring.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableBinding(ProducerChannels.class)
@RestController
public class ProducerApplication {
    
    private final MessageChannel testChannel;
    
    public ProducerApplication(ProducerChannels channels) {
        this.testChannel = channels.test();
    }

    @GetMapping("/publish/{message}")
    public void publish(@PathVariable String message) {
        testChannel.send(MessageBuilder.withPayload(message).build());
    }
    
	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}
}

interface ProducerChannels {

    @Output
    MessageChannel test();

}
