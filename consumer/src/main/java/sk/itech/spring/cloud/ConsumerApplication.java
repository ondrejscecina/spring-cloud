package sk.itech.spring.cloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@SpringBootApplication
@EnableBinding(ConsumerChannels.class)
public class ConsumerApplication {
    
    private final SimpMessagingTemplate messageTemplate;
    
    public ConsumerApplication(SimpMessagingTemplate messageTemplate) {
        this.messageTemplate = messageTemplate;
    }
    
    @Bean
    @Scope("prototype")
    Logger logger(InjectionPoint ip) {
        return LoggerFactory.getLogger(ip.getDeclaredType().getName());
    }
    
    @Bean
    IntegrationFlow integrationFlow(
            Logger logger,
            ConsumerChannels channels) {

        return IntegrationFlows
                .from(channels.test())
                .handle(String.class, (payload, headers) -> {
                    logger.info("Message:{}", payload);
                    messageTemplate.convertAndSend("/topic/messages", new Message(payload));
                    return null;
                })
                .get();
    }

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}

}

interface ConsumerChannels {
    
    @Input
    SubscribableChannel test();
    
}

@Configuration
@EnableWebSocketMessageBroker
class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/gs-guide-websocket").withSockJS();
    }

}

class Message {

    private String content;

    public Message() {}

    public Message(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

}
