# Microservices in action.

```
                                    +-----------+
GET /public/{message}               |           |
                                    | Service C | ---> Supplier's system
      |                             |           |
      |                             +-----------+
      v                                   ^
+-----------+      +---------+            |
|           |      |         | -----------+
| Service A | ---> |  Kafka  |
|           |      |         | -----------+
+-----------+      +---------+            |
                                          v
                                    +-----------+
                                    |           |
                                    | Service B |
                                    |           |
                                    +-----------+
                                          |
                                          |
                                          v
                                       Browser
                                     (WebSocket)
```


Service A receives a message (i.e. create the order). In this example it is a simple
REST endpoint. We want to do something when the order is created successfully.
Maybe send an email, a message to a supplier's system... We do not want
to do it as a part of the Service A as it is not its responsibility. We could send
a message directly to the Service B, but if we wanted to add more (to send the
message to a buyer's financial system, etc.) things would get a bit more complicated.
Instead we want Service B to update the web page only and have other services that
would send emails or talk to other systems. We do not want Service A to know about
these services as adding more functionality would mean updating the Service A
as well with the list of new services.

Let us send an event to a message broker (Kafka, RabbitMQ...) instead. We will
probably have other services that will listen to incoming messages. These services
have no idea who is producing the events. Also Service A does not know that there
are other services that are consuming these events. In case we need to add more
functionality in the future, we just need to deploy the new service (without
updating the Service A).



Instructions

1. Kafka.
  * Download the binary file from the https://kafka.apache.org/downloads URL
  * Extract it.
  * Start ZooKeper server ```./bin/zookeeper-server-start.sh config/zookeeper.properties```
  * Start Kafka server ```./bin/kafka-server-start.sh config/server.properties```
  * (see https://kafka.apache.org/quickstart for more information)

2. Clone this project ```git clone https://gitlab.com/ondrejscecina/spring-cloud.git```

3. Run the producer ```ProducerApplication.java```

4. Run the consumer ```ConsumerApplication.java```

5. Open the http://localhost:9010/index.html page in your browser and click "Connect"
to create the WebSocket connection

6. Send the message ```curl http://localhost:9000/publish/My%20first%20message```

7. Check the index.html for the new message.
